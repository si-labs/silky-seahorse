// Project related
export const baseName = 'Service Innovation Labs'
export const legalName = 'SI Labs GmbH'
export const shortName = 'SI Labs'
export const baseUrl = 'http://www.si-labs.com'
export const baseUrlSecure = 'https://www.si-labs.com'
export const metaTitleDivider = ' | '
export const metaTitleAppendix = baseName
export const color = '#e50134'
export const colorBg = '#ffffff'

// meta fallback
export const metaDescription = ''
export const metaTitle = baseName

// social media
export const twitterId = '473906937'
export const twitterHandle = 'si_labs' // whithout @
export const twitterUrl = 'https://www.twitter.com/si_labs'
export const facebookUrl = 'https://www.facebook.com/serviceinnovationlabs/'
export const facebookId = '289399864448672'
export const linkedinUrl = 'https://www.linkedin.com/company/si-labs-gmbh/'
export const instagramUrl = 'https://www.instagram.com/si_labs/'

// *** //

// Storyblok - project-related
export const sbPublicToken = '9dQ0hHFBPXrc4YCJkoT4Bgtt'
export const sbPreviewToken = 'ce5eWJafPr3gbgvLQyhr6Att'
export const sbIndexPageId = '829261' // home page id (to exlude it from bein generated twice)
export const sbPageComponents = 'PageGeneral,PageArticle,PageCasestudy' // IMPORTANT! Add all Components that should be generated as pages (comma-separated without space. In Case we ever have more than 100 Pages that should be generated, we'll need to paginate the API Call for nuxt generate)

// *** //

// Storyblok - general
const useProductionVersion = () => {
  if (
    process.env.NODE_ENV == 'development' ||
    process.env.STORYBLOK_MODE == 'preview'
  ) {
    return false
  } else {
    return true
  }
}
export const sbAccessToken = useProductionVersion()
  ? sbPublicToken
  : sbPreviewToken
const sbVersion = useProductionVersion() ? 'published' : 'draft'

const sbApiUrl = 'https://api.storyblok.com/v1/cdn/stories'
export const sbGetUrl =
  sbApiUrl +
  '?version=' +
  sbVersion +
  '&token=' +
  sbAccessToken +
  '&cv=' +
  Math.floor(Date.now() / 1e3)
export const sbGetUrlProd =
  sbApiUrl +
  '?version=' +
  'published' +
  '&token=' +
  sbPublicToken +
  '&cv=' +
  Math.floor(Date.now() / 1e3)
