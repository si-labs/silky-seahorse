// https://github.com/ymmooot/nuxt-jsonld
import Vue from 'vue'
import NuxtJsonld from 'nuxt-jsonld'

Vue.use(NuxtJsonld)

Vue.use(NuxtJsonld, {
  // you can set the indentation
  space: process.env.NODE_ENV === 'production' ? 0 : 2 // default: 2
})
