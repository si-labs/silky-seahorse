export default function({ app, store }) {
  // Every time the route changes (fired on initialization too)
  app.router.afterEach((to, from) => {
    // close menu after each route change
    store.dispatch('closeMenu')
  })
}
