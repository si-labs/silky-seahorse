window.pipedriveLeadboosterConfig = {
  base: 'leadbooster-chat.pipedrive.com',
  companyId: 5628750,
  playbookUuid: '46bab4b9-21c2-4e81-8c66-cfc1cc2a3f54',
  version: 2
}

/*       
Add this to nuxt config > scripts:
{
  src: 'https://leadbooster-chat.pipedrive.com/assets/loader.js',
  async: true
} */
