import Vue from 'vue'

// use in templates as {{ $log(message) }}
Vue.prototype.$log = console.log

Vue.prototype.$getLocalStorage = function(key) {
  if (process.browser) {
    return localStorage.getItem(key)
  }
}

Vue.prototype.$setLocalStorage = function(key, value) {
  if (process.browser) {
    localStorage.setItem(key, value)
  }
}
