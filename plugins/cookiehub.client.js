window.addEventListener('load', function() {
  window.cookieconsent.initialise({
    onInitialise: function(status) {
      if (this.hasConsented('required')) {
      }
      if (this.hasConsented('analytics')) {
      }
      if (this.hasConsented('marketing')) {
      }
    },
    onAllow: function(category) {
      if (category == 'required') {
      }
      if (category == 'analytics') {
      }
      if (category == 'marketing') {
      }
    },
    onRevoke: function(category) {
      if (category == 'required') {
      }
      if (category == 'analytics') {
      }
      if (category == 'marketing') {
      }
    }
  })
})

/* {
        src: 'https://cookiehub.net/cc/c7955b1a.js',
        async: true
      }, */
