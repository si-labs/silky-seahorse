// import pkg from './package'
import path from 'path'
import * as constants from './constants.js'
import axios from 'axios'

export default {
  target: 'static',
  env: {
    // TODO: disable live on netlify
    STORYBLOK_MODE: 'preview'
  },

  /*
   ** Headers of the page
   */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    title: constants.metaTitle,
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'preconnect',
        href: 'http://img2.storyblok.com/',
        crossorigin: 'anonymous'
      },
      {
        rel: 'preconnect',
        href: 'https://img2.storyblok.com/',
        crossorigin: 'anonymous'
      },
      {
        rel: 'preconnect',
        href: 'http://a.storyblok.com/',
        crossorigin: 'anonymous'
      },
      {
        rel: 'preconnect',
        href: 'https://a.storyblok.com/',
        crossorigin: 'anonymous'
      }
      /* {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com/',
        crossorigin: 'anonymous'
      }, */
      /* {
        rel: 'preconnect',
        href: 'https://connect.facebook.net',
        crossorigin: 'anonymous'
      }, */
    ],
    script: []
  },

  /*
   ** Customize the loading indicator
   */
  loading: '~/components/nuxt/loading.vue',

  /*
   ** Global CSS
   */
  css: ['~/assets/css/tailwind.css', '~/assets/css/main.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/global.js',
    '~/plugins/globalComponents.js',
    '~/plugins/filters.js',
    '~/plugins/router.js',
    '~/plugins/jsonld.js',
    '~/plugins/v-lazy-image.js',
    '~/plugins/vue-js-modal.js',
    '~/plugins/vue-observe-visibility.js'
    // '~/plugins/mouseflow.client.js',
    // '~/plugins/pipedrive-leadbooster.client.js'
    // '~/plugins/cookiehub.client.js',
    // '~/plugins/facebookPixel.client.js',
    // '~plugins/ga.client.js',
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Nuxt polyfill https://github.com/Timkor/nuxt-polyfill
    'nuxt-polyfill',
    // Axios: https://axios.nuxtjs.org/
    '@nuxtjs/axios',
    // PWA functions
    '@nuxtjs/pwa',
    // RSS Feed Module: https://github.com/nuxt-community/feed-module
    '@nuxtjs/feed',
    // PurgeCSS
    'nuxt-purgecss',
    // https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources',
    // Smooth scroll to page internal ids https://github.com/rigor789/vue-scrollto
    ['vue-scrollto/nuxt', { duration: 300, offset: 32 }],
    // Storyblok https://github.com/storyblok/storyblok-nuxt
    [
      'storyblok-nuxt',
      {
        accessToken: constants.sbAccessToken
      }
    ],
    // Tracking with Matomo/Piwik https://github.com/pimlie/nuxt-matomo
    ['nuxt-matomo', { matomoUrl: '//silabs.innocraft.cloud/', siteId: 1 }],
    // Http2 Server Push with netlify https://github.com/jmblog/nuxt-netlify-http2-server-push/
    [
      'nuxt-netlify-http2-server-push',
      {
        // Specify relative path to the dist directory and its content type
        resources: [
          { path: '**/*.js', as: 'script' },
          {
            path: 'assets/fonts/*.woff2',
            as: 'font',
            type: 'font/woff2',
            crossorigin: 'anonymous'
          }
        ]
      }
    ],
    // Sentry.io Error Tracking
    '@nuxtjs/sentry',
    // Sitemap.xml (NEEDS TO BE LAST MODULE!) https://www.npmjs.com/package/@nuxtjs/sitemap
    '@nuxtjs/sitemap'
  ],
  buildModules: [
    // Use Svgs like components https://github.com/egoist/svg-to-vue-component
    'svg-to-vue-component/nuxt'
  ],
  /*
   ** Router Config
   */
  router: {
    /* routes: [
      {
        path: '/index',
        redirect: '/'
      }
    ], */
    middleware: 'getSettings'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    postcss: {
      // https://nuxtjs.org/faq/postcss-plugins/
      plugins: {
        tailwindcss: path.resolve(__dirname, './tailwind.config.js')
      },
      preset: {
        // Change the postcss-preset-env settings
        stage: 0,
        autoprefixer: {
          cascade: false // don't indent prefixes
          // grid: 'autoplace' // enable IE polyfill for css grid
        },
        cssnano: {
          preset: 'default',
          discardComments: { removeAll: true },
          zindex: false
        }
      }
    },
    extend(config, ctx) {}
  },

  /*
   * Render configuration
   */
  render: {
    http2: { push: true }
  },
  /*
   * Generate configuration
   */
  generate: {
    routes: function() {
      return axios
        .get(
          constants.sbGetUrlProd +
          '&filter_query[component][in]=' +
          constants.sbPageComponents + // get all components that should be generated as pages
          '&excluding_ids=' +
          constants.sbIndexPageId + // exclude /index page (so it is not generated twice)
            '&per_page=100&page=1' // IMPORTANT: once we have more than 100 entries, pagination will be neccesary! https://forum.storyblok.com/t/how-to-generate-routes-for-nuxt-js-with-storyblok/105
        )
        .then(res => {
          const pages = res.data.stories.map(page => page.full_slug)
          return [
            '/', // home page
            ...pages // pages from storyblok
          ]
        })
    }
  },
  /*
   * module-specific settings
   */
  purgeCSS: {
    // CSS tree shaking
    // Doc: https://github.com/Developmint/nuxt-purgecss
    mode: 'postcss',
    whitelist: [
      'is-active',
      'nuxt-link-exact-active',
      'nuxt-link-active',
      'hidden',
      'block',
      'xs:hidden',
      'xs:block',
      'sm:hidden',
      'sm:block',
      'md:hidden',
      'md:block',
      'lg:hidden',
      'lg:block',
      'xl:hidden',
      'xl:block'
    ], // https://github.com/FullHuman/purgecss-docs/blob/master/whitelisting.md
    whitelistPatterns: [/page/]
  },
  styleResources: {
    // Global scss vars and mixins
    scss: ['~/assets/css/mixins.scss']
  },
  feed: [
    /*
     * RSS Feed
     * https://github.com/nuxt-community/feed-module
     * https://github.com/jpmonette/feed
     * https://www.storyblok.com/tp/how-to-generate-an-rss-feed-with-a-headless-cms
     */
    {
      path: '/feed.xml',
      async create(feed) {
        feed.options = {
          title: constants.baseName,
          language: 'de',
          link: constants.baseUrl,
          description: constants.description
        }

        const { data } = await axios.get(
          constants.sbGetUrlProd +
            '&filter_query[component][in]=' +
            'PageArticle,PageCasestudy' // get all components that should be available in feed
        )

        data.stories.forEach(post => {
          feed.addItem({
            title: post.content.title ? post.content.title : post.name,
            link: post.content.is_external
              ? post.content.external_link
              : constants.baseUrl + '/' + post.full_slug,
            description: post.content.excerpt ? post.content.excerpt : null
          })
        })

        feed.addCategory('Design')
        feed.addCategory('Business')
        feed.addCategory('New Work')
        feed.addCategory('Company Culture')
      },
      cacheTime: 1000 * 60 * 15,
      type: 'rss2'
    }
  ],
  sitemap: {
    hostname: constants.baseUrlSecure,
    gzip: true,
    exclude: ['/admin/**', '/test'],
    // add trailing slash to routes:
    filter({ routes }) {
      return routes.map(route => (route.url = `${route.url}/`))
    }
  },
  manifest: {
    name: constants.baseName,
    short_name: constants.shortName,
    lang: 'de',
    // display: 'minimal-ui',
    display: 'standalone',
    theme_color: constants.colorBg,
    background_color: constants.color,
    description: constants.metaDescription,
    scope: '/',
    start_url: '/'
  },
  polyfill: {
    features: [
      {
        require: 'intersection-observer',
        detect: () => 'IntersectionObserver' in window
      }
    ]
  },
  sentry: {
    dsn: 'https://f4d02e6b58b345a6aa99d3f6b1007bd8@sentry.io/1521667', // Enter your project's DSN here
    config: {} // Additional config
  }
}
