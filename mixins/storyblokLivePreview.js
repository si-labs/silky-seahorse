export default {
  mounted() {
    this.$storybridge.on(['published', 'change'], event => {
      if (!event.slugChanged) {
        // Reload the page on save events
        // + Autosave Plugin
        this.$nuxt.$router.go({
          path: this.$nuxt.$router.currentRoute,
          force: true,
        })
      }
    })
    // disable the input event when using resolve_relations, since it causes the preview to error
    /* this.$storybridge.on(['input'], event => {
      if (event.story.id === this.story.id) {
        this.story.content = event.story.content
      }
    }) */
  },
}
