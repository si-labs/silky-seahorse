# TODO:

## BUGs

- temporary workaround for now.sh build in package.json:
  "resolutions": {
  "execa": "<=2.0.0"
  }

- put in package.json:?

  ```
  "engines": {
    "node": "10.x"
  }
  ```

  https://github.com/nuxt/now-builder/issues/101

## Do Now

- add cms-managed classes field to more components
- AppPicture:

  - option to show different image based on screen size (responsive image art direction)
  - lazyload svgs in `<img>`
  - Add option to define aspect ratio to prevent loading jumps

- paginate blog and case entries
- change meta author on Articles
- Event Banner
- stories/?starts_with= für Faces on About page (or wait for Nuxt3.0?)
- fix page internal navigation in storyblok preview with Now / Drafted content is not shown in frontend after navgating in page
- add `sizes` to AppPicture?
- NetlifyForm.vue component (see bottom)
- Pro-active livechat (Replacement for Pipedrive Leadbooster)
  - https://landbot.io
  - https://crisp.chat/
- recreate Landingpages /hallo /finance /moblity

## Do Later

- In Case we ever have more than 100 Pages that should be generated, we'll need to paginate the API Call like this: https://forum.storyblok.com/t/how-to-generate-routes-for-nuxt-js-with-storyblok/105
- https://github.com/wearewondrous/nuxt-storyblok-router / https://github.com/wearewondrous/nuxt-storyblok-queries
- Publishing Flow => 4 Augen Prinzip
- Micro-Animations: https://useanimations.com/
- Performance:
  - https://github.com/maoberlehner/vue-lazy-hydration
- A/B testing
  - https://github.com/fromAtoB/vue-a2b
  - feature not available in current matomo plan
  - storyblok branches?
- ideally links in .md should be <AppLink>

---

## Add Blog posts:

### Old

- http://www.si-labs.com/blog/2015/5/19/design-to-align-to-different-cultural-contexts
- https://www.si-labs.com/blog/2015/5/4/designing-services-for-the-financial-industry

---

## Howto: clickable live preview in Storyblok:

- Nuxt min. v2.4
- Ein now.json im root folder mit content: https://gist.github.com/DominikAngerer/9f2c43c72de5ce29cddc2d6909642074
- im nuxt.config.js sicherstellen den preview token zu verwenden
- nuxt build ausführen
- now deploy ausführen -> https://zeit.co/now schon habt ihr eine preview Umgebung

---

```
<template>
  <form
    :name="'form-' + _uid"
    method="post"
    action="/thank-you/"
    data-netlify="true"
    data-netlify-honeypot="bot-field"
    class=""
  >
    <input type="hidden" name="form-name" :value="'form-' + _uid" />
    <slot />
  </form>
</template>
```
