const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
      rotate: {
        '-28': '-28deg'
      },
      minWidth: {
        '0': '0',
        '1/4': '25%',
        '1/2': '50%',
        full: '100%'
      },
      maxWidth: {
        xxs: '16rem'
      }
    },
    colors: {
      foreground: 'var(--c-foreground)',
      background: 'var(--c-background)',
      highlight: 'var(--c-highlight)',
      black: 'var(--c-darkest)',
      white: 'var(--c-lightest)',
      grey: 'var(--c-grey)',
      'grey-light': 'var(--c-grey-light)',
      red: 'var(--c-red)',
      yellow: 'var(--c-yellow)',
      'blue-dark': 'var(--c-blue-dark)',
      'blue-light': 'var(--c-blue-light)'
    },
    screens: {
      xs: '440px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px'
    },
    container: {
      center: true
    }
  },
  variants: {
    // ...
  },
  plugins: [
    require('tailwindcss-typography')({
      ellipsis: true, // defaults to true
      hyphens: true, // defaults to true
      textUnset: true // defaults to true
    })
  ]
}
