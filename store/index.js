import { Store } from 'vuex'

export const state = () => ({
  cacheVersion: '',
  settings: {
    main_nav: []
  },
  articles: [],
  cases: [],
  // language: 'de',
  menuIsOpen: false,
  showShareButtons: false
})

// mutations (commit)
export const mutations = {
  setSettings(state, settings) {
    state.settings = settings
  },
  // setLanguage(state, language) {
  //   state.language = language
  // },
  setCacheVersion(state, version) {
    state.cacheVersion = version
  },
  setMenuStatus(state, status) {
    state.menuIsOpen = status === true ? true : false
  },
  setShareButtonsVisbility(state, status) {
    state.showShareButtons = status === true ? true : false
  },
  setArticles(state, value) {
    state.articles = value
  },
  setCases(state, value) {
    state.cases = value
  }
}

// actions (dispatch)
export const actions = {
  loadSettings({ commit, state }, context) {
    if (state.settings.main_nav.length == 0) {
      return (
        this.$storyapi
          // resolve_links: 'url' // not working?
          .get(`cdn/stories/_globalsettings`, {
            version: context.version
          })
          .then(res => {
            commit('setSettings', res.data.story.content)
          })
      )
    }
  },
  toggleMenu({ commit, state }) {
    if (state.menuIsOpen === false) {
      commit('setMenuStatus', true)
    } else {
      commit('setMenuStatus', false)
    }
  },
  closeMenu({ commit }) {
    commit('setMenuStatus', false)
  },
  openMenu({ commit }) {
    commit('setMenuStatus', true)
  }
}
