# Project Silky Seahorse

> next version of si-labs.com

## Build with:

- [Nuxt.js](https://nuxtjs.org/) - The Vue.js App framework
- [Storyblok CMS](https://www.storyblok.com/) - Headless Component-Based CMS with a Visual Editor
- [Tailwind](https://tailwindcss.com/) - Utility-first CSS framework

## Features:

- [PurgeCSS](https://github.com/Developmint/nuxt-purgecss) (CSS Tree Shaking) - Gets rid of all unused CSS
- Progressive Web App Basics with [@nuxtjs/pwa](https://pwa.nuxtjs.org/)
- Sitemap generation with [@nuxtjs/sitemap](https://github.com/nuxt-community/sitemap-module)
- RSS Feed generation (WIP) with [@nuxtjs/feed](https://github.com/nuxt-community/feed-module)
- globally available scss mixins/variables/etc [@nuxtjs/style-resources](https://github.com/nuxt-community/style-resources-module)

## Build Setup

```bash
# install NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
# install and use current node version
nvm install 12
# install yarn globally
npm i -g yarn
# install dependencies
$ yarn
# serve with hot reload at localhost:3000 for development
$ yarn dev

```

Other commands:

```
// Analyze webpack bundle:
yarn build -a

// deploy statically
yarn generate
```

## Useful Links:

- [Landesdatenschutzbeauftragter BW zu Cookie Banner & Tracking](https://www.baden-wuerttemberg.datenschutz.de/faq-zu-cookies-und-tracking-2/)
- [How to write image alt texts](https://axesslab.com/alt-texts/)
- Storyblok
  - https://www.storyblok.com/tp/headless-cms-nuxtjs
  - https://www.storyblok.com/tp/nuxt-js-multilanguage-website-tutorial
  - https://www.storyblok.com/docs/api/content-delivery
  - https://www.storyblok.com/docs/image-service
- Material Rounded icons: https://material.io/tools/icons/?icon=email&style=round
